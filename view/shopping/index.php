<?php
session_start();

$_SESSION['clientes'] = ['id' => 15];

require '../../controller/ProdutosController.php';

$produtos = (new ProdutosController())->index();

require "../layout/header.php";
?>

<div class="col-md-12">
    <br>
    <div class="row">

        <div class="col-md-10">
            <h2>Shopping</h2>
            <br>

        </div>
        <div class="col-md-2">

            <a href="../home/home.php" type="button" class="btn btn-secondary pull-rigth">Voltar</a>
            <a href="confirm.php" type="button" class="btn btn-success pull-rigth">Finalizar</a>

        </div>
    </div>
    <div class="row">

        <?php foreach ($produtos as $produto) { ?>

            <div class="col-md-3" style="margin-bottom: 2%;">
                <div class="card" style="width: 16rem;">
                    <img src="/product-image-placeholder.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <form action="../../controller/CartCore.php/Cart/cart" method="POST">
                            <input style="display:none" name="id" value="<?= $produto->id ?>">
                            <h5 class="card-title"><?= $produto->name ?></h5>
                            <p class="card-text"><?= $produto->description ?></p>
                            <div class="row">
                                <button type="submit" class="btn btn-primary">Comprar</button>
                                <h3 style="margin-left: 35%;"><?= number_format($produto->price, 2, ",", ".") ?></h3>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
</div>
<?php require "../layout/footer.php"; ?>