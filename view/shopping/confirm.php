<?php
require '../../controller/CartController.php';

require "../layout/header.php";
?>
<div class="col-md-12">
    <br>
    <div class="row">

        <div class="col-md-11">

            <h2>Carrinho</h2>
            <br>
        </div>
        <div class="col-md-1">
            <a href="pedidos.php" type="button" class="btn btn-success pull-rigth">Finalizar</a>

        </div>
    </div>

    <table class="table table-bordered table-dark">
        <caption>Version 1.0</caption>
        <thead>
            <tr>
                <th scope="col">Produtos</th>
                <th scope="col">Quantidade</th>
                <th scope="col">Preço</th>
                <th scope="col">Total Unitáio</th>
                <th colspan="2" width="1%">Opção</th>
            </tr>
        </thead>
        <tbody>
            <?php $valorTotal = 0;

            $carts = (new CartController())->checkOut();

            if ($carts) {

                foreach ($carts as $cart) {

                    $valorTotal += $cart['amount'] * $cart['price'] ?>
                    <tr>
                        <td><?= $cart['name'] ?></td>
                        <td><?= $cart['amount'] ?></td>
                        <td>R$ <?= number_format($cart['price'], 2, ",", ".") ?></td>
                        <td>R$ <?= number_format($cart['amount'] * $cart['price'], 2, ",", ".") ?></td>

                        <td>
                            <form action="../../controller/CartCore.php/Cart/altProductSession" method="post">
                                <input type="hidden" name="id" value="<?= $cart['id'] ?>">
                                <input type="hidden" name="add" value="-">

                                <button type="submit" class="btn btn-info">

                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-minus">
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                </button>

                            </form>
                        </td>
                        <td>
                            <form action="../../controller/CartCore.php/Cart/altProductSession" method="post">
                                <input type="hidden" name="id" value="<?= $cart['id'] ?>">
                                <input type="hidden" name="add" value="+">

                                <button type="submit" class="btn btn-info">

                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus">
                                        <line x1="12" y1="5" x2="12" y2="19"></line>
                                        <line x1="5" y1="12" x2="19" y2="12"></line>
                                    </svg>
                                </button>

                            </form>
                        </td>
                    </tr>

            <?php }
            } ?>
            <tr style="background-color: #808080;">
                <td colspan="3"><strong>Valor Total</strong></td>
                <td colspan="3">R$ <?= number_format($valorTotal, 2, ",", ".") ?></td>
            </tr>

        </tbody>
    </table>
    <a href="index.php" type="button" class="btn btn-secondary pull-rigth">Voltar</a>
</div>

<?php require "../layout/footer.php"; ?>