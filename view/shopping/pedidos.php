<?php
require '../../controller/CartController.php';

require "../layout/header.php"
?>
<div class="col-md-12">
    <br>
    <div class="row">

        <div class="col-md-11">

            <h2>Meus Pedidos</h2>
            <br>
        </div>
    </div>

    <table class="table table-bordered table-dark">
        <caption>Version 1.0</caption>
        <thead>
            <tr>
                <th scope="col" >Pedido 000001 | Aguardando Pagamento</th>
                <th scope="col">Total Unitário</th>
            </tr>
        </thead>
        <tbody>
            <?php $valorTotal = 0;

            $pedidos = (new CartController())->myOrders();

            if ($pedidos) {

                foreach ($pedidos as $pedido) {

                    $valorTotal += $pedido['amount'] * $pedido['price'] ?>
                    <tr>
                        <td><?= $pedido['name'] ?></td>
                        <td>R$ <?= number_format($pedido['amount'] * $pedido['price'], 2, ",", ".") ?></td>
                    </tr>

            <?php }
            } ?>
            <tr style="background-color: #808080;">
                <td colspan=""><strong>Valor Total</strong></td>
                <td colspan="1">R$ <?= number_format($valorTotal, 2, ",", ".") ?></td>
            </tr>

        </tbody>
    </table>
    <a href="index.php" type="button" class="btn btn-secondary pull-rigth">Voltar</a>
</div>

<?php require "../layout/footer.php";
