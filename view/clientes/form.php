<?php

@$id = $_GET['id'];

if ($id) {
    include "../../controller/ClientesController.php";

    $clientes = new ClientesController();
    $clientes = $clientes->edit($id);
}
require "../layout/header.php" ?>


<div class="col-md-12">
    <br>
    <form action="../../controller/ClientesCore.php/Clientes/add" method="post">
        <input type="hidden" name="id" value="<?= @$clientes['id'] ?>">
        <div class="col-md-12">
            <h2>Cadastro de Cliente</h2>
            <br>
            <label>Nome</label>
            <input type="text" name="name" class="form-control" value="<?= @$clientes['name'] ?>">
        </div>
        <div class="col-md-12">
            <label>CPF / CNPJ</label>
            <input type="text" name="cpf_cnpj" class="form-control" value="<?= @$clientes['cpf_cnpj'] ?>">
        </div>
        <div class="col-md-12">
            <br>
            <hr>
            <a href="index.php" type="button" class="btn btn-secondary">Voltar</a>
            <button type="submit" class="btn btn-success float-right">Salvar</button>
        </div>
    </form>
</div>

<?php require "../layout/footer.php";
