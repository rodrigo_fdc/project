<?php

@$id = $_GET['id'];

if ($id) {
    include "../../controller/ProdutosController.php";

    $produtos = new ProdutosController();
    $produtos = $produtos->edit($id);
}

require "../layout/header.php";
?>

<div class="col-md-12">
    <br>
    <form action="../../controller/ProdutosCore.php/Produtos/add" method="post">
        <input type="hidden" name="id" value="<?= @$produtos['id'] ?>">

        <div class="col-md-12">
            <h2>Cadastro de Produto</h2>
            <br>
            <label>Nome</label>
            <input type="text" name="name" class="form-control" value="<?= @$produtos['name'] ?>">
        </div>
        <div class="col-md-12">
            <label>Preço</label>
            <input type="text" name="price" class="form-control" value="<?= number_format(@$produtos['price'], 2, ",", ".") ?>">
        </div>
        <div class="col-md-12">
            <label>Descrição</label>
            <textarea type="text" name="description" rows="3" class="form-control"><?= @$produtos['description'] ?></textarea>
        </div>
        <div class="col-md-12">
            <br>
            <hr>
            <a href="index.php" type="button" class="btn btn-secondary">Voltar</a>
            <button type="submit" class="btn btn-success float-right">Salvar</button>
        </div>
    </form>
</div>

<?php require "../layout/footer.php";
