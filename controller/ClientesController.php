<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include '../model/clientesModel.php';
} else {
    include '../../model/clientesModel.php';
}

class ClientesController
{
    protected $clientesModel;

    public function __construct()
    {
        $this->clientesModel = new ClientesModel();
    }

    public function index()
    {
        return $this->clientesModel->list();
    }
    public function add()
    {
        $id = $_POST['id'];

        if ($id) {
            $this->insert();
        } else {
            $this->update();
        }
    }
    public function list()
    {
        header('Location:  /view/clientes/index.php');
    }
    public function insert()
    {
        if ($_POST['name'] != "" && $_POST['cpf_cnpj'] != "") {

            $this->save();
        } else {

            header('Location:  /view/clientes/form.php');
        }
    }
    public function update()
    {
        if ($_POST['name'] != "" && $_POST['cpf_cnpj'] != "") {

            $this->save();
        } else {
            header('Location:  /view/clientes/index.php');
        }
    }
    public function edit($id = null)
    {
        return $this->clientesModel->get($id);
    }
    public function save()
    {
        $id = $_POST['id'];

        $store = [
            'name'     => $_POST['name'],
            'cpf_cnpj' => $_POST['cpf_cnpj']
        ];

        if ($this->clientesModel->store($id, $store)) {
            header('Location:  /view/clientes/index.php');
        } else {
            header('Location:  /view/clientes/form.php');
        }
    }

    public function delete()
    {
        $id = $_POST['id'];

        if ($this->clientesModel->delete($id)) {
            header('Location:  /view/clientes/index.php');
        }
    }
}
