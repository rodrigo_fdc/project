<?php

require "ClientesController.php";

$url = $_SERVER['REQUEST_URI'];

$url = explode('/', $url);
$url = array_slice($url, 1);

$controller = $url[2] . "Controller";
$action = $url[3];

call_user_func_array(array(new $controller, $action), array());
