<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include '../model/produtosModel.php';
} else {
    include '../../model/produtosModel.php';
}

class ProdutosController
{
    protected $produtosModel;

    public function __construct()
    {
        $this->produtosModel = new ProdutosModel();
    }

    public function index()
    {
        return $this->produtosModel->list();
    }
    public function add()
    {
        $id = $_POST['id'];

        if ($id) {
            $this->insert();
        } else {
            $this->update();
        }
    }
    public function insert()
    {
        if ($_POST['name'] != "" && $_POST['price'] != "" && $_POST['description'] != "") {

            $this->save();
        } else {

            header('Location:  /view/produtos/form.php');
        }
    }
    public function update()
    {
        if ($_POST['name'] != "" && $_POST['price'] != "" && $_POST['description'] != "") {

            $this->save();
        } else {
            header('Location:  /view/produtos/index.php');
        }
    }
    public function edit($id = null)
    {
        return $this->produtosModel->get($id);
    }
    public function save()
    {
        $id = $_POST['id'];

        $store = [
            'name'     => $_POST['name'],
            'price' => $_POST['price'],
            'description' => $_POST['description']
        ];

        if ($this->produtosModel->store($id, $store)) {
            header('Location:  /view/produtos/index.php');
        } else {
            header('Location:  /view/produtos/form.php');
        }
    }

    public function delete()
    {
        $id = $_POST['id'];

        if ($this->produtosModel->delete($id)) {
            header('Location:  /view/produtos/index.php');
        }
    }
}
