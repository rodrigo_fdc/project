<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include '../model/cartModel.php';
} else {
    include '../../model/cartModel.php';
}

class CartController
{
    protected $cartModel;

    public function __construct()
    {
        $this->cartModel = new CartModel();
    }

    public function cart()
    {
        $id = $_POST['id'];

        session_start();

        $qtd =  (isset($_SESSION['cart-' . $id])) ? $_SESSION['cart-' . $id]['amount'] + 1 :  1;

        $_SESSION['cart-' . $id] =
            [
                'produtos_id' => $id,
                'clientes_id' => $_SESSION['clientes']['id'],
                'amount'      => $qtd
            ];

        header('Location:  /view/shopping/index.php');
    }

    public function checkOut()
    {
        session_start();

        if (!empty($_SESSION)) {

            $array = array();

            foreach ($this->cartModel->produtos() as $produto) {

                if (isset($_SESSION['cart-' . $produto->id]['produtos_id'])) {

                    $obj = [
                        "id" => $produto->id,
                        "name" => $produto->name,
                        "price" => $produto->price,
                        "amount" => $_SESSION['cart-' . $produto->id]['amount']
                    ];

                    array_push($array, $obj);
                }
            }
            return $array;
        }
    }

    public function altProductSession()
    {
        session_start();

        $id = $_POST['id'];
        $add = $_POST['add'];

        if ($add == "+") {
            $qtd =  $_SESSION['cart-' . $id]['amount'] + 1;
        } else {
            $qtd =  $_SESSION['cart-' . $id]['amount'] - 1;
        }

        $_SESSION['cart-' . $id] =
            [
                'produtos_id' => $id,
                'clientes_id' => 15,
                'amount'      => $qtd
            ];

        if ($_SESSION['cart-' . $id]['amount'] == 0) {

            unset($_SESSION['cart-' . $id]);
        }

        header('Location:  /view/shopping/confirm.php');
    }

    public function myOrders()
    {
        session_start();

        if (!empty($_SESSION)) {

            $array = array();

            foreach ($this->cartModel->produtos() as $produto) {

                if (isset($_SESSION['cart-' . $produto->id]['produtos_id'])) {

                    $obj = [
                        "id" => $produto->id,
                        "name" => $produto->name,
                        "price" => $produto->price,
                        "amount" => $_SESSION['cart-' . $produto->id]['amount']
                    ];

                    array_push($array, $obj);
                }
            }
            return $array;
        }
    }
}
