<?php
include 'connect.php';

class CartModel
{
    protected $db;
    protected $pdo;

    public function __construct()
    {
        $this->db  = new Connect();
        $this->pdo = $this->db->connect();
    }

    public function produtos()
    {
        $query = "SELECT * FROM produtos";

        $STH = $this->pdo->prepare($query);
        $STH->execute();

        return $STH->fetchAll(PDO::FETCH_OBJ);
    }
}
