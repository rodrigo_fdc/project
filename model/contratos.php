<?php

class Query
{
    protected $db;
    protected $pdo;

    public function __construct()
    {
        /*
        *Instância da conexão com DB
        */
        $this->db  = new DB();
        $this->pdo = $this->db->connect();
    }

    /*
    * @tipo_parcela
    * equivale ao tipo 'Contratos';
    *
    * @tipo_envio
    * é o parâmetro =
       [ '1' => 'vence em breve',
         '2' => 'vence hoje',
         '3' => 'em aberto'
       ]    
    */

    public function getParcelas($parametro, $data)
    {
        $sinal = ($parametro == 3) ? '<=' : '=';

        $query = "SELECT turmas_alunos.alunos_id AS alunos_id, turmas_alunos.turmas_id AS turmas_id
                  FROM unidades
                    
                    JOIN instituicoes ON unidades.id = instituicoes.unidades_id
                    JOIN turmas ON instituicoes.id = turmas.instituicoes_id
                    JOIN turmas_alunos ON turmas.id = turmas_alunos.turmas_id
                    JOIN parcelas ON turmas_alunos.alunos_id = parcelas.alunos_id
                        
                        WHERE turmas_alunos.id NOT IN 
                        (SELECT turmas_alunos_id FROM historico_email WHERE data LIKE :buscaData AND tipo_envio = :parametro AND tipo_parcela = 1)
                            
                            AND parcelas.status_pagamento NOT IN (1, 3, 4)

                            AND parcelas.data $sinal :data
                            
                            ANd unidades.empresas_id = 2
                            AND turmas_alunos.status = 1
                            AND parcelas.valor > 50 

                            GROUP BY parcelas.alunos_id, parcelas.turmas_id LIMIT 1";

        $STH = $this->pdo->prepare($query);

        $buscaData = "%" . date('Y-m') . "%";
        $STH->bindParam(':data', $data);
        $STH->bindParam(':parametro', $parametro, PDO::PARAM_INT);
        $STH->bindParam(':buscaData', $buscaData, PDO::PARAM_STR);
        $STH->execute();
        // $STH->debugDumpParams();

        return $STH->fetchAll(PDO::FETCH_OBJ);
    }

    public function getInformacoesFormandos($alunos_id, $turmas_id, $parametro, $data)
    {
        $sinal = ($parametro == 3) ? '<=' : '=';

        $query = "SELECT DISTINCT contratos.nome as cNome, parcelas.numero, parcelas.valor, turmas_alunos.alunos_id AS alunos_id, 
                        alunos.nome, alunos.email, parcelas.id AS parcelas_id, turmas_alunos.turmas_id, turmas_alunos.id AS turmas_alunos_id
                 FROM alunos
                    
                    JOIN turmas_alunos ON alunos.id = turmas_alunos.alunos_id
                    JOIN turmas ON turmas_alunos.turmas_id = turmas.id
                    JOIN contratos ON turmas.contratos_id = contratos.id
                    JOIN parcelas ON turmas_alunos.alunos_id = parcelas.alunos_id
                        
                        WHERE parcelas.data $sinal :data

                            AND parcelas.alunos_id = :alunos_id
                            AND parcelas.turmas_id = :turmas_id
                            AND parcelas.status_pagamento NOT IN (1, 3, 4)
                            AND turmas_alunos.status = 1
                            AND parcelas.valor > 50";

        $STH = $this->pdo->prepare($query);

        $STH->bindParam(':alunos_id', $alunos_id);
        $STH->bindParam(':turmas_id', $turmas_id);
        $STH->bindParam(':data', $data);

        $STH->execute();
        // $STH->debugDumpParams();

        return $STH->fetch(PDO::FETCH_ASSOC);
    }

    public function quantParcelasAtrazadas($alunos_id, $turmas_id)
    {
        $query = "SELECT COUNT(numero) AS quant FROM parcelas 
                    WHERE turmas_id = :turmas_id 
                        AND alunos_id = :alunos_id";

        $STH = $this->pdo->prepare($query);

        $STH->bindParam(':alunos_id', $alunos_id);
        $STH->bindParam(':turmas_id', $turmas_id);

        $STH->execute();
        // $STH->debugDumpParams();

        return $STH->fetch(PDO::FETCH_ASSOC);
    }

    public function parcelasAtrazadas($alunos_id, $turmas_id, $data)
    {
        $query = "SELECT id, valor, numero, data FROM parcelas
                    WHERE alunos_id = :alunos_id
                        AND turmas_id = :turmas_id
                        AND status_pagamento NOT IN (1, 3, 4)
                        AND valor > 50
                        AND data <= :data
                            ORDER BY data ASC";

        $STH = $this->pdo->prepare($query);

        $STH->bindParam(':alunos_id', $alunos_id);
        $STH->bindParam(':turmas_id', $turmas_id);
        $STH->bindParam(':data', $data);

        $STH->execute();
        // $STH->debugDumpParams();

        return $STH->fetchAll(PDO::FETCH_OBJ);
    }

    public function insertHistopricoEmail($alunos_id, $parcelas_id, $parametro)
    {
        $data = [
            'parcelas_id'     => $parcelas_id,
            'turmas_alunos_id' => $alunos_id,
            'data'            => date('Y-m-d'),
            'enviado'         => 1,
            'tipo_parcela'    => 1,             // parâmetro de contratos
            'tipo_envio'      => $parametro,    // tipo referente ao tipo de cobrança
        ];

        $SQL = "INSERT INTO historico_email (parcelas_id, turmas_alunos_id, data, enviado, tipo_parcela, tipo_envio) 
                                     VALUES (:parcelas_id, :turmas_alunos_id, :data, :enviado, :tipo_parcela, :tipo_envio)";

        $STH = $this->pdo->prepare($SQL);
        $STH->execute($data);
    }

    public function verificaStatusAlunoAtivo($turmas_id, $alunos_id)
    {
        $query = "SELECT * FROM turmas_alunos 
                    WHERE turmas_id = :turmas_id
                        AND alunos_id = :alunos_id
                            AND status = 1";

        $STH = $this->pdo->prepare($query);

        $STH->bindParam(':turmas_id', $turmas_id, PDO::PARAM_INT);
        $STH->bindParam(':alunos_id', $alunos_id, PDO::PARAM_INT);
        $STH->execute();
        // $STH->debugDumpParams();

        return $STH->fetch(PDO::FETCH_ASSOC);
    }
}
