<?php

include 'connect.php';

class ProdutosModel
{
    protected $db;
    protected $pdo;

    public function __construct()
    {
        $this->db  = new Connect();
        $this->pdo = $this->db->connect();
    }

    public function get($id)
    {
        $query = "SELECT * FROM produtos WHERE id = :id";

        $STH = $this->pdo->prepare($query);
        $STH->bindParam(':id', $id, PDO::PARAM_INT);
        $STH->execute();

        return $STH->fetch(PDO::FETCH_ASSOC);
    }

    public function list()
    {
        $query = "SELECT * FROM produtos";

        $STH = $this->pdo->prepare($query);
        $STH->execute();

        return $STH->fetchAll(PDO::FETCH_OBJ);
    }

    public function store($id = null, $data = null)
    {
        if ($id) {
            $query = "UPDATE produtos SET name = :name, price = :price, description = :description WHERE id = :id";

            $data += ['id' => $id];
        } else {
            $query = "INSERT INTO produtos (name, price, description) VALUES (:name, :price, :description)";
        }

        $STH = $this->pdo->prepare($query);

        // $STH->execute($data);
        // $STH->debugDumpParams();

        if ($STH->execute($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $query = "DELETE FROM produtos WHERE id = :id";

        $STH = $this->pdo->prepare($query);

        $STH->bindParam(':id', $id, PDO::PARAM_INT);
        if ($STH->execute()) {
            return true;
        } else {
            return false;
        }
        // $STH->debugDumpParams();
    }
}
