<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
class Mailer
{

    public function loadTemplate($templateUrl)
    {
        $content = "";

        $handle = fopen($templateUrl, "r");
        while (!feof($handle)) {
            $content .= fgets($handle);
        }
        return $content;
    }

    public function changeContent($content, $name, $var)
    {
        return str_replace($name, $var, $content);
    }

    public function sendmail($toMail, $toName, $subject, $altbody, $content, $fromMail, $fromName, $toRepply)
    {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                   // Enable verbose debug output
            $mail->isSMTP();                                           // Send using SMTP
            $mail->Host       = $_ENV['SMTP_HOST'];                    // Set the SMTP server to send through
            $mail->SMTPAuth   = $_ENV['SMTP_AUTH'];                    // Enable SMTP authentication
            $mail->Username   = $_ENV['SMTP_USER'];                    // SMTP username
            $mail->Password   = $_ENV['SMTP_PASS'];                    // SMTP password
            $mail->SMTPSecure = $_ENV['SMTP_SECURE'];                  // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = $_ENV['SMTP_PORT'];                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            $mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');

            //Recipients
            $mail->setFrom($fromMail, $fromName);
            $mail->addAddress($toMail, $toName);                       // Add a recipient
            //$mail->addAddress('ellen@example.com');                  // Name is optional
            $mail->addReplyTo($toRepply, $subject);
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            // Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');            // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');       // Optional name

            // Content
            $mail->isHTML(true);                                       // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $content;
            $mail->AltBody = $altbody;
            $mail->CharSet = 'UTF-8';

            $mail->send();
            echo ":::SEND " . $toMail . " HAS BEEN SENT::: \n\n";

            $mail->clearAttachments();
            $mail->clearAllRecipients();

            return true;
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo} \n";

            return false;
        }
    }
}
